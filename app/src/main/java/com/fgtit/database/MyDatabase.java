package com.fgtit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fgtit.model.EmployeeAuthDetailsItem;
import com.fgtit.model.EmployeeDetailsItem;

import java.util.ArrayList;
import java.util.HashMap;

public class MyDatabase extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;

    public MyDatabase(Context applicationcontext) {
        super(applicationcontext, "attendance.db", null, DB_VERSION);
    }

    private static final String CREATE_EMPLOYEE_MASTER_TABLE =
            "CREATE TABLE \"tblEmployeeDetails\" (\"EmployeeId\" INTEGER PRIMARY KEY ,\"EmployeeName\" VARCHAR," +
                    " \"EmployeeRole\" VARCHAR,\"Password\" VARCHAR,\"EmployeeRfid\" VARCHAR,\"RegistrationTime\" VARCHAR, \"Status\" VARCHAR)";

    private static final String CREATE_EMPLOYEE_DETAILS_TABLE =
            "CREATE TABLE \"tblEmployeeAuthDetails\" (\"EmployeeId\" INTEGER PRIMARY KEY ,\"AuthType\" VARCHAR," +
                    " \"AuthData\" VARCHAR, \"Status\" VARCHAR)";

    private static final String CREATE_ATTENDANCE_LOG_TABLE =
            "CREATE TABLE \"tblAttendaceLogs\" (\"EmployeeId\" INTEGER PRIMARY KEY ,\"Finger1Id\" INTEGER," +
            " \"Finger1Data\" VARCHAR,\"Finger2Id\" INTEGER,\"Finger2Data\" VARCHAR," + "\"DeviceId\" VARCHAR)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_EMPLOYEE_MASTER_TABLE);
        sqLiteDatabase.execSQL(CREATE_EMPLOYEE_DETAILS_TABLE);
        sqLiteDatabase.execSQL(CREATE_ATTENDANCE_LOG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertEmployeeDetails(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("EmployeeId", queryValues.get("EmployeeId"));
        values.put("EmployeeName", queryValues.get("EmployeeName"));
        values.put("EmployeeRole", queryValues.get("EmployeeRole"));
        values.put("Password", queryValues.get("Password"));
        values.put("EmployeeRfid", queryValues.get("EmployeeRfid"));
        values.put("RegistrationTime", queryValues.get("RegistrationTime"));
        values.put("Status", queryValues.get("Status"));

        database.insert("tblEmployeeDetails", null, values);
        database.close();
    }

    public void insertEmployeeAuthDetails(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("EmployeeId", queryValues.get("EmployeeId"));
        values.put("AuthType", queryValues.get("AuthType"));
        values.put("AuthData", queryValues.get("AuthData"));
        values.put("Status", queryValues.get("Status"));

        database.insert("tblEmployeeAuthDetails", null, values);
        database.close();
    }

//    public void insertAttendanceLog(HashMap<String, String> queryValues) {
//        SQLiteDatabase database = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("EmployeeId", queryValues.get("EmployeeId"));
//        values.put("Finger1Id", queryValues.get("Finger1Id"));
//        values.put("Finger1Data", queryValues.get("Finger1Data"));
//        values.put("Finger2Id", queryValues.get("Finger2Id"));
//        values.put("Finger2Data", queryValues.get("Finger2Data"));
//        values.put("DeviceId", queryValues.get("DeviceId"));
//        values.put("Status", queryValues.get("Status"));
//
//        database.insert("tblEmployeeDetails", null, values);
//        database.close();
//    }

    public ArrayList<EmployeeDetailsItem> getAllEmployeeDetails() {
        ArrayList<EmployeeDetailsItem> employeeDetailsList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  tblEmployeeDetails where Status = 1";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                EmployeeDetailsItem n = new EmployeeDetailsItem();
                n.setEmployeeId(cursor.getString(0));
                n.setEmployeeName(cursor.getString(1));
                n.setEmployeeRole(cursor.getString(2));
                n.setEmployeePassword(cursor.getString(3));
                n.setEmployeeRFID(cursor.getString(4));

                employeeDetailsList.add(n);
            } while (cursor.moveToNext());
        }
        return employeeDetailsList;
    }

    public ArrayList<EmployeeAuthDetailsItem> getAllEmployeeAuthDetails() {
        ArrayList<EmployeeAuthDetailsItem> employeeDetailsList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  tblEmployeeAuthDetails where Status = 1";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                EmployeeAuthDetailsItem n = new EmployeeAuthDetailsItem();
                n.setEmployeeId(cursor.getString(0));
                n.setAuthType(cursor.getString(1));
                n.setAuthData(cursor.getString(2));

                employeeDetailsList.add(n);
            } while (cursor.moveToNext());
        }
        return employeeDetailsList;
    }

    public int getActiveEmployeesCount(){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT employeeId FROM  tblEmployee where Status = 1";
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }
}
